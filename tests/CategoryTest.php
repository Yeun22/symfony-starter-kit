<?php

namespace App\Tests;

use App\Entity\Category;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Attributes\CoversClass;

#[CoversClass(Category::class)]
class CategoryTest extends TestCase
{
    public function testCategoryIsTrue(): void
    {
        $category = new Category();
        $category->setName('test')
            ->setDescription('test')
            ->setUpdatedAt(new \DateTimeImmutable());

        $this->assertTrue($category->getName() === 'test');
        $this->assertTrue($category->getDescription() === 'test');
        $this->assertTrue($category->getCreatedAt() instanceof \DateTimeImmutable);
        $this->assertTrue($category->getUpdatedAt() instanceof \DateTimeImmutable);

    }

    public function testCategorySetCreatedAt(): void
    {
        $now = new \DateTimeImmutable();

        $category = new Category();
        $category->setName('test2')
            ->setDescription('test2')
            ->setCreatedAt($now);

        $this->assertTrue($category->getCreatedAt() === $now);

    
    }
}
