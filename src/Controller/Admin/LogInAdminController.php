<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class LogInAdminController extends AbstractController
{
    #[Route('/login/admin', name: 'admin_login')]
    public function index() : Response
    {
        return $this->render('admin/login.html.twig');
    }
}