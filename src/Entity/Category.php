<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\CategoryRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[ApiResource]
#[ORM\HasLifecycleCallbacks]
class Category
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $updatedAt = null;

    #[ORM\OneToMany(targetEntity: Exemple::class, mappedBy: 'category', orphanRemoval: true)]
    private Collection $exemples;

    public function __construct()
    {
        $this->createdAt = new DateTimeImmutable();
        $this->exemples = new ArrayCollection();
    }

    #[ORM\PreFlush]
    public function setUpdatedAtValue(): void {
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeImmutable $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Exemple>
     */
    public function getExemples(): Collection
    {
        return $this->exemples;
    }

    public function addExemple(Exemple $exemple): static
    {
        if (!$this->exemples->contains($exemple)) {
            $this->exemples->add($exemple);
            $exemple->setCategory($this);
        }

        return $this;
    }

    public function removeExemple(Exemple $exemple): static
    {
        if ($this->exemples->removeElement($exemple)) {
            // set the owning side to null (unless already changed)
            if ($exemple->getCategory() === $this) {
                $exemple->setCategory(null);
            }
        }

        return $this;
    }
}
